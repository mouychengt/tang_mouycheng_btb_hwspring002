package com.example.model;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;

public class Subject {
    private int id;
//    @NotEmpty(message = "Title cannot be empty..")
    private String title;
//    @NotEmpty(message = "Description cannot be empty..")
    private String description;

    private String profile;
    public Subject(int id, String title, String description,String profile) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.profile = profile;
    }
    public Subject(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}