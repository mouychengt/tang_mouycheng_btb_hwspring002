package com.example.service;

import com.example.model.Subject;
import org.springframework.stereotype.Service;

import java.util.List;

    @Service
    public interface SubjectService {
        public List<Subject> getAllSubject();
        public Subject findSubjectByID(int id );
        public void subjectMethod();
        public void AddSubjectMethod(Subject subject);
    }
