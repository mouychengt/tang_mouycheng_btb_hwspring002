package com.example.service.ServiceImp;

import com.example.model.Subject;
import com.example.repository.SubjectRepository;
import com.example.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectServiceImp implements SubjectService {

    @Autowired
    SubjectRepository subjectRepository;

    @Override
    public List<Subject> getAllSubject() {
        return subjectRepository.getAllSubject();
    }
    @Override
    public Subject findSubjectByID(int id) {
        return subjectRepository.findSubjectByID(id);
    }


    @Override
    public void subjectMethod() {
        System.out.println("Version one the student method.");
    }
    @Override
    public void AddSubjectMethod(Subject subject) {
        subjectRepository.addNewSubject(subject);
    }
}
