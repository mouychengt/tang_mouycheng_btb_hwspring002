package com.example.controller;

import com.example.model.Subject;
import com.example.service.FileStorageService;
import com.example.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;

@Controller
public class SubjectController {

    @Autowired
    @Qualifier("subjectServiceImp")

    SubjectService subjectService;
    // 3 methods : field, setter, constructor

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping
    public String index(Model model){

//        System.out.println("here is the value of all the students :");
//       studentService.getAllStudents().stream().forEach(System.out::println);
//
        model.addAttribute("subject", new Subject());
        model.addAttribute("subjects",subjectService.getAllSubject());
        return "index";


    }


    @GetMapping("/add")
    public String showAdd(Model model){

        model.addAttribute("subject",new Subject());
        return "add";
    }

    @PostMapping("/handle-add")
    public String handleAdd(@ModelAttribute @Valid Subject subject ,BindingResult bindingResult, Errors error){

        if (subject.getFile().isEmpty()){
            System.out.println("File is empty ");
        }

        else {

            try{
                System.out.println("File :  "+ subject.getFile().getContentType());
                String filename = "http://localhost:1111/images/"+fileStorageService.saveFile(subject.getFile());
                System.out.println("filename: "+filename);
                subject.setProfile(filename);

            }catch (IOException ex){
                System.out.println("Error with the image upload "+ex.getMessage());
            }

        }

        System.out.println("Here is result value : "+subject);


        if (error.hasErrors()){
            System.out.println("Something is wrong ..... ");
            return "/add";
        }
        subjectService.AddSubjectMethod(subject);


        return  "redirect:/";
    }




    @GetMapping("/view/{id}")
    public String viewSubject(@PathVariable int id,Model model){
        // Find Student By ID

        Subject resultSubject = subjectService.findSubjectByID(id);

        System.out.println("result subject : "+resultSubject);
        // send value to the view
        model.addAttribute("subject",resultSubject);

        return "view";
    }
}
